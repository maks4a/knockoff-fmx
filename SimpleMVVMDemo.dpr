program SimpleMVVMDemo;

uses
  Forms,
  MainView in 'MainView.pas' {MainViewForm},
  MainViewModel in 'MainViewModel.pas',
  Knockoff.Binding.Components in 'Knockoff.Binding.Components.pas',
  Knockoff.Binding in 'Knockoff.Binding.pas',
  Knockoff.Observable in 'Knockoff.Observable.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainViewForm, MainViewForm);
  Application.Run;
  ReportMemoryLeaksOnShutdown := True;
end.
