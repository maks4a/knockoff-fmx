unit FMX.MainView;

interface

uses
  FMX.Knockoff.Binding,
  System.SysUtils,
  System.Types,
  System.UITypes,
  System.Classes,
  System.Variants,
  FMX.Types,
  FMX.Controls,
  FMX.Forms,
  FMX.Graphics,
  FMX.Dialogs,
  FMX.Edit,
  FMX.Controls.Presentation,
  FMX.ListBox,
  MainViewModel,
  FMX.StdCtrls,
  FMX.EditBox,
  FMX.SpinBox;

type
  TForm2 = class(TForm)
    grp1: TGroupBox;
    [Bind('Text', 'FirstName')]
    edtFirstName: TEdit;
    [Bind('Text', 'LastName')]
    edtLastName: TEdit;
    [Bind('Text', 'FullName')]
    lblFullName: TLabel;
//
    grp2: TGroupBox;
    [Bind('Text', 'NumberOfClicks')]
    lblClickCount: TLabel;
    [Bind('Click', 'RegisterClick')]
    [Bind('Disabled', 'HasClickedTooManyTimes')]
    btnRegisterClick: TButton;
    [Bind('Visible', 'HasClickedTooManyTimes')]
    lblClickedTooManyTimes: TLabel;
    [Bind('Click', 'ResetClicks')]
    [Bind('Visible', 'HasClickedTooManyTimes')]
    btnResetClicks: TButton;
//
    GroupBox3: TGroupBox;
    [Bind('Value', 'ChosenTicket')]
    [BindOptions('Tickets')]
    [BindOptionsCaption('Choose...')]
    [BindOptionsText('Name')]
    cbTickets: TComboBox;
    [Bind('Click', 'ResetTicket')]
    [Bind('Enabled', 'ChosenTicket')]
    btnClear: TButton;
    [Bind('Text', 'ChosenTicket.Price')]
    lblPrice: TLabel;
    grp3: TGroupBox;
//
    [Bind('Value', 'Country')]
    [BindOptions('AvailableCountries')]
    [BindOptionsCaption('Choose...')]
    cbAvailableCountries: TComboBox;
    spnbx1: TSpinBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

uses
  Knockoff.Observable,
  FMX.Knockoff.Binding.Components,
  Rtti;


{$R *.fmx}
var
  vm: TViewModel;

type
  TSpinEditBinding = class(TBinding<TSpinBox>)
  protected
    procedure HandleChange(Sender: TObject);
    function InitGetValue(const Observable: IObservable): TFunc<TValue>; override;
    procedure InitTarget; override;
  end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  vm := TViewModel.Create('John', 'Doe');
  ApplyBindings(Self, vm);
  TSpinEditBinding.Create(spnbx1, vm.Number as IObservable);
end;

{ TSpinEditBinding }

procedure TSpinEditBinding.HandleChange(Sender: TObject);
begin
  Source.Value := Target.Text.ToSingle;
end;

function TSpinEditBinding.InitGetValue(const observable: IObservable): TFunc<TValue>;
begin
  Result :=
    function: TValue
    var
      x: integer;
    begin
      x := observable.Value.AsInteger;
      Target.Value := x;
    end;
end;

procedure TSpinEditBinding.InitTarget;
begin
  Target.OnChange := HandleChange
end;

end.

